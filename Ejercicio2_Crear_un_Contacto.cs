using System;

namespace Contacto
{
    public class Contacto
    {
        public string N1; //Nombre
        public string A2; //Apelllido
        public int T3; //Telefono
        public string D4; //Direccion
        public void SetContacto(String N1)
        {
            this.N1 = N1;
        }
        public void SetContacto1(String A2)
        {
            this.A2 = A2;
        }

        public void SetContacto2(int T3)
        {
            this.T3 = T3;
        }

        public void SetContacto3(string D4)
        {
            this.D4 = D4;
        }

        public void Saludar()
        {
            Console.WriteLine("Hola Soy " + N1 + " " + A2 + ", Vivo en " + D4 + ", Mi Número de telefono es " + T3 );
        }

        public class ProbarContacto
        {

            public static void Main()
            {
                Contacto cont1 = new Contacto();
                Contacto cont2 = new Contacto();
                cont1.SetContacto("Emmanuel ");
                cont1.SetContacto1("Caceres ");
                cont1.SetContacto2(849274817);
                cont1.SetContacto3("San Pedro");
                cont2.SetContacto("Ava ");
                cont2.SetContacto1("Matt");
                cont2.SetContacto2(2047562836);
                cont2.SetContacto3("Miami ");
                cont1.Saludar();
                cont2.Saludar();
            }
        }
    }
}
  
