﻿using System;

namespace ejercicio222
{
    public class A
    {
        public A(int a)
        {
            Console.WriteLine("Ejemplo de Ejercicio Matematico : ");
            Console.Write(a);
        }
    }

    public class B : A
    {
        public B(int b) : base(b / 2)
        {
            Console.Write(" Más " + 5);
            Console.Write(", Es Igual a " + b);
        }
    }

    public class C : B
    {
        public C(int c) : base(c / 2)
        {
            Console.WriteLine(". Multiplicado X " + 2 +  " Es Igual a " + c);
        }
    }

    class Control 
    {
        static void Main(string[] args)
        {
            C obj1 = new C(20);
            Console.ReadKey();
        }
    }
}
